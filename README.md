
## Clonar repositorio en Bitbucket

En tu consola ejecuta el siguiente comando:

*git clone https://lucasguzman@bitbucket.org/lucasguzman/apitasks.git*

## Crear Base de datos

En tu Localhost crea una BBDD que se llame "apitasks", con un username "root" y sin password

## Copia el archivo .env

En la consola ejecuta: *cp .env.example .env*

De esta manera tendras la configuración lista para instalar las dependencias de Composer y de Node

## Composer

Ejecutar: *composer install*

## npm

Ejecutar: *npm install*

Luego: *npm run dev*

## Ejecutar Migraciones y Seeder

Ejecutar : *php artisan migrate --seed*

## Levantar servidor local

En tu consola ejecuta: *php artisan serve*

Ten en cuenta que deberás tener instalado en tu máquina PHP version 7.4 o superior

ingresa en la url que levanta Artisan, por lo general es: http://127.0.0.1:8000

## Saludos!!

Muchas gracias por todo, Saludos cordiales... Lucas Guzmán
