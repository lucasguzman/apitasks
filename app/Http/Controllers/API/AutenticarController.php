<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AccesoRequest;
use App\Http\Requests\RegistroRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AutenticarController extends Controller
{
    /**
    * @OA\Post(
    *     path="/api/acceso",
    *     summary="Loguear Usuario",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Genera el token de acceso y lo devuelve. El mismo sirve para el resto de las consultas de tareas",
    *     ),
    *     @OA\Response(
    *         response="422",
    *         description="Los datos proporcionados no son válidos."
    *     ),
    * )
    */
    public function acceso(AccesoRequest $request){
        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'msg' => ['Las credenciales son incorrectas.'],
            ]);
        }

        $token =  $user->createToken($request->email)->plainTextToken;

        return response()->json([
            'res' => true,
            'token' => $token
        ], 200);
    }

    /**
    * @OA\Post(
    *     path="/api/registro",
    *     summary="Crear Nuevo Usuario",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Crear un nuevo usuario."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function registro(RegistroRequest $request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'res' => true,
            'msg' => 'Usuario registrado correctamente!'
        ], 200);
    }

    /**
    * @OA\Post(
    *     path="/api/cerrarsesion",
    *     summary="Desloguear Usuario",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Elimina el token de acceso."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function cerrarSesion(Request $request){

        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'res' => true,
            'msg' => 'Token eliminado correctamente'
        ], 200);
    }
}
