<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

/**
* @OA\Info(title="API Tareas", version="1.0")
*
* @OA\Server(url="http://127.0.0.1:8000")
*/
class TasksController extends Controller
{
    /**
    * @OA\Get(
    *     path="/api/tasks",
    *     summary="Mostrar tareas",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="Bearer {token}"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Muestra todas las tareas."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function index()
    {
        return TaskResource::collection(Task::orderByDesc('created_at')->paginate(5));
    }

    /**
    * @OA\Post(
    *     path="/api/tasks",
    *     summary="Crea una nueva tarea",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="Bearer {token}"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Devuelve la nueva tarea creada."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function store(StoreTaskRequest $request)
    {
        return (new TaskResource(Task::create($request->all())))->additional([
            'msg' => 'Tarea creada correctamente!'
        ]);
    }

    /**
    * @OA\Get(
    *     path="/api/tasks/{task}",
    *     summary="Mostrar una tarea específica",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="Bearer {token}"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Retorna una sola tarea."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function show(Task $task)
    {
        return new TaskResource($task);
    }

    /**
    * @OA\Put(
    *     path="/api/tasks/{task}",
    *     summary="Actualizar tarea",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="Bearer {token}"
    *         )
    *     ),
    *     @OA\Response(
    *         response=202,
    *         description="Actuliza la informacion de una tarea."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $task->update($request->all());

        return (new TaskResource($task))->additional([
            'msg' => 'Tarea Actualizada correctamente!'
        ])
        ->response()
        ->setStatusCode(202);
    }

    /**
    * @OA\Delete(
    *     path="/api/tasks/{task}",
    *     summary="Elimina una tarea",
    *     @OA\Parameter(
    *         name="Accept",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Content-Type",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="application/json"
    *         )
    *     ),
    *     @OA\Parameter(
    *         name="Authorization",
    *         in="header",
    *         description="cabecera requerida",
    *         required=true,
    *         @OA\Schema(
    *             type="string",
    *             default="Bearer {token}"
    *         )
    *     ),
    *     @OA\Response(
    *         response=202,
    *         description="Elimina definitivamente una tarea."
    *     ),
    *     @OA\Response(
    *         response="default",
    *         description="Ha ocurrido un error."
    *     )
    * )
    */
        public function destroy(Task $task)
    {
        $task->delete();

        return (new TaskResource($task))->additional([
            'msg' => 'Tarea Eliminada correctamente!'
        ]);
    }
}
