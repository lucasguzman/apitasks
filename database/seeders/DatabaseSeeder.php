<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => '25 Whatts',
            'email' => 'admin@admin.com',
            'password' => bcrypt('12345678')
        ]);
        Task::factory(30)->create();
    }
}
